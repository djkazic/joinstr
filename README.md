# joinstr

### coinjoin implementation using nostr

![joinstr-new-logo](https://i.imgur.com/UseTcdC.png)

**Requirements:**

 - [python-nostr](https://github.com/jeffthibault/python-nostr) for web app and electrum plugin
 - [nostr-sdk](https://pypi.org/project/nostr-sdk/) for cli
 - Bitcoin Core for web app and cli

**Using electrum plugin**

- Install [nostrj](https://pypi.org/project/nostrj/)

```
pip install nostrj
```
- Install openvpn
- Copy plugin files and run electrum

**Docs:** [https://docs.joinstr.xyz](https://docs.joinstr.xyz)

**TO DO:**

- [ ] Do not allow registering different types of inputs for a round.
- [ ] Use NIP 9 to delete events after round is completed.
- [ ] Create an Android app.

