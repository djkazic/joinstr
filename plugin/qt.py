import json
import ssl
import time
import random
import string
import base64
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QTabWidget, QWidget, QMessageBox, QVBoxLayout, QLineEdit, QPushButton, QLabel, QHBoxLayout, QFormLayout, QListWidget, QListWidgetItem, QGroupBox, QProgressBar, QDialog
from electrum.i18n import _
from electrum.gui.qt.util import WindowModalDialog
from electrum.plugin import BasePlugin, hook
from PyQt5.QtCore import Qt, pyqtSignal, QObject, QTimer
from electrum.transaction import Transaction, PartialTransaction, PartialTxInput, PartialTxOutput
from nostr.event import Event, EncryptedDirectMessage
from nostr.relay_manager import RelayManager
from nostr.message_type import ClientMessageType
from nostr.filter import Filter, Filters
from nostr.key import PrivateKey
import threading
import subprocess
import requests
import os

class SignalEmitter(QObject):
    show_inputlist_signal = pyqtSignal(QWidget, QWidget, object, QLabel)

class Plugin(BasePlugin):

    def __init__(self, parent, config, name):
        BasePlugin.__init__(self, parent, config, name)
        self.signal_emitter = SignalEmitter()
        self.address_text_fields = {}

    def requires_settings(self):
        return True

    def settings_widget(self, window):
        btn = QtWidgets.QPushButton(_("Settings"))
        btn.clicked.connect(lambda: self.settings_dialog(window))
        return btn

    def settings_dialog(self, window):
        d = WindowModalDialog(window, _("joinstr config"))
        self.d = d
        layout = QtWidgets.QVBoxLayout(d)

        settings = [
            {"label": _("Nostr Relay:"), "key": "nostr_relay"},
            {"label": _("Pool Denomination:"), "key": "pool_denomination", "validator": self.float_validator},
            {"label": _("Number of peers:"), "key": "num_peers", "validator": self.integer_validator}
        ]

        for setting in settings:
            label = QtWidgets.QLabel(setting["label"])
            edit = QtWidgets.QLineEdit()
            edit.setText(self.config.get(setting["key"], ''))
            edit.setFixedWidth(300)
        
            if "validator" in setting:
                validator = setting["validator"]()
                edit.setValidator(validator)
        
            setattr(self, f"{setting['key']}_edit", edit) 
            layout.addWidget(label)
            layout.addWidget(edit)

        ok_button = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok, d)
        layout.addWidget(ok_button)
        ok_button.accepted.connect(lambda: self.ok_clicked(
            self.nostr_relay_edit.text(),
            self.pool_denomination_edit.text(),
            self.num_peers_edit.text()
        ))
        d.exec_()

    def float_validator(self):
        return QtGui.QDoubleValidator(bottom=0.0, decimals=8)

    def integer_validator(self):
        return QtGui.QIntValidator(bottom=1)

    def ok_clicked(self, relay, pool_denomination, num_peers):

        self.config.set_key('nostr_relay', relay, True)
        self.config.set_key('pool_denomination', pool_denomination, True)
        self.config.set_key('num_peers', num_peers, True)
        self.d.accept()

    def connect_vpn(self, config):
        process = subprocess.Popen(['sudo', 'openvpn', '--config', config], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        vpn_connected = False

        for line in process.stdout:
            if 'Initialization Sequence Completed' in line:
                response = requests.get('https://ifconfig.me')
                public_ip = response.text.strip()
                print(f"[joinstr plugin] Connected to VPN. Public IP: {public_ip}")
                vpn_connected = True
                break

        process.wait()

        if not vpn_connected:
            print("Error: VPN connection failed")
            print(process.stderr.read())

    @hook
    def load_wallet(self, wallet, window):
        if not self.is_enabled():
            return

        config_url = "https://gitlab.com/1440000bytes/joinstr/-/raw/main/config.vpn"
        response = requests.get(config_url)
        if response.status_code == 200:
            config_content = response.text
        else:
            print(f"Error: Unable to fetch VPN configuration. Status Code: {response.status_code}")
            return

        config= 'joinstr_config.vpn'
        config_path = os.path.expanduser(os.path.join('~', '.electrum', config))
        with open(config_path, 'w') as config_file:
            config_file.write(config_content)
        
        connect_vpn_thread = threading.Thread(target=self.connect_vpn, args=(config_path,))
        connect_vpn_thread.daemon = True
        connect_vpn_thread.start()

        tab_widget = QTabWidget()
        tab_widget.setObjectName("coinjoin_tab")

        widget = QWidget()
        layout = QVBoxLayout(widget)
        pools_widget = QWidget()
        pools_layout = QVBoxLayout(pools_widget)

        form_layout = QFormLayout()

        label = QLabel("Enter a new address:")
        self.address_text_fields[wallet] = QLineEdit()
        self.address_text_fields[wallet].setFixedWidth(500)
        submit_button = QPushButton("Register")
        submit_button.setFixedWidth(100)

        create_pool_button = QPushButton("Create new pool")
        create_pool_button.setFixedWidth(200)
        my_pool_button = QPushButton("My pools")
        my_pool_button.setFixedWidth(200)
        view_pool_button = QPushButton("View other pools")
        view_pool_button.setFixedWidth(200)

        '''
        My pools
        '''
        pool_path = os.path.expanduser(os.path.join('~', '.electrum', 'pools.json'))
        if os.path.exists(pool_path) and os.stat(pool_path).st_size > 0:
            with open(pool_path, 'r') as file:
                try:
                    my_pools_data = json.load(file)
                    my_pools_data.reverse()
                except Exception as e:
                    print(f"Error loading pools from JSON: {e}")
                    my_pools_data = []

        '''
        Other pools
        '''
        event_type ='new_pool'
        event_kind = 10942

        other_pools_data = self.getevents(event_type, event_kind)[0]
        
        create_pool_button.clicked.connect(lambda: self.show_create_pool_form(tab_widget))
        my_pool_button.clicked.connect(lambda: self.show_pools_form(tab_widget, my_pools_data, join=False))
        view_pool_button.clicked.connect(lambda: self.show_pools_form(tab_widget, other_pools_data, join=True))

        pools_widget = QWidget()
        pools_layout = QVBoxLayout(pools_widget)

        pools_layout.addWidget(create_pool_button)
        pools_layout.addWidget(my_pool_button)
        pools_layout.addWidget(view_pool_button)

        field_button_layout = QHBoxLayout()
        field_button_layout.addWidget(self.address_text_fields[wallet])
        field_button_layout.addWidget(submit_button)

        form_layout.addRow(label, field_button_layout)

        layout.addStretch(1)
        layout.addLayout(form_layout)
        layout.addStretch(1)
        
        '''
        tab_widget.addTab(widget, _("Output Registration"))
        '''        
        tab_widget.addTab(pools_widget, _("Pools")) 

        window.tabs.addTab(tab_widget, _("Coinjoin"))

        submit_button.clicked.connect(lambda: self.register_output(wallet, self.address_text_fields[wallet].text(), tab_widget))

        self.signal_emitter = SignalEmitter()
        
        self.signal_emitter.show_inputlist_signal.connect(self.show_inputlist)

    def show_create_pool_form(self, tab_widget):
        tab_layout = tab_widget.widget(0).layout()

        if hasattr(self, 'form_groupbox'):
            self.form_groupbox.setParent(None)

        create_pool_widget = QWidget()
        create_pool_layout = QVBoxLayout(create_pool_widget)

        denomination_label = QLabel("Denomination:")
        denomination_edit = QLineEdit()
        denomination_edit.setText(self.config.get('pool_denomination', ''))

        num_peers_label = QLabel("Number of peers:")
        num_peers_edit = QLineEdit()
        num_peers_edit.setText(self.config.get('num_peers', ''))

        create_button = QPushButton("Create")
        create_button.setFixedWidth(100)

        create_pool_layout.addWidget(denomination_label)
        create_pool_layout.addWidget(denomination_edit)
        create_pool_layout.addWidget(num_peers_label)
        create_pool_layout.addWidget(num_peers_edit)
        create_pool_layout.addWidget(create_button)

        self.form_groupbox = QGroupBox()
        self.form_groupbox.setLayout(create_pool_layout)

        tab_layout.addWidget(self.form_groupbox)

        create_button.clicked.connect(lambda: self.create_channel(denomination=denomination_edit.text(), peers=num_peers_edit.text(), timeout=600))

    def show_pools_form(self, tab_widget, pools_data, join=None):
        tab_layout = tab_widget.widget(0).layout()

        if hasattr(self, 'form_groupbox'):
            self.form_groupbox.setParent(None)

        my_pools_widget = QWidget()
        my_pools_layout = QVBoxLayout(my_pools_widget)

        pools_list_widget = QListWidget()
        for index, pool in enumerate(pools_data):
            try:
                if 'timeout' in pool and pool['timeout'] <= int(time.time()):                
                    continue
                pool_str = f"Denomination: {pool['denomination']}, Peers: {pool['peers']}, Relay: {pool['relay']}, Pubkey: {pool['public_key']}"
                pool_list_item = QListWidgetItem(pool_str)
                pools_list_widget.addItem(pool_list_item)
                if index == 0 and join == False: 
                    pool_list_item.setBackground(QColor('yellow'))
                    pool_list_item.setForeground(QColor('black'))
            except Exception as e:
                print(f"Error processing pool item: {e}")

        if not pools_list_widget.count():        
            no_pools_label = QLabel("No pools found")
            my_pools_layout.addWidget(no_pools_label)
            self.form_groupbox = QGroupBox()
            self.form_groupbox.setLayout(my_pools_layout)
            tab_layout.addWidget(self.form_groupbox)
            return

        join_button = QPushButton("Join")
        join_button.setFixedWidth(100)

        my_pools_layout.addWidget(pools_list_widget)
        if join == True:
            my_pools_layout.addWidget(join_button)
            
        self.form_groupbox = QGroupBox()
        self.form_groupbox.setLayout(my_pools_layout)

        tab_layout.addWidget(self.form_groupbox)

        join_button.clicked.connect(lambda: self.join_channel(request={"type": "join_pool"}, pubkey=pools_data[pools_list_widget.currentRow()]["public_key"], relay=pools_data[pools_list_widget.currentRow()]["relay"]))

    def create_channel(self, denomination, peers, timeout):

        letters = string.ascii_lowercase
        pool_id = ''.join(random.choice(letters) for i in range(10)) + str(int(time.time()))

        private_key = PrivateKey()
        pub_key=private_key.public_key.hex()
        channel = {"type": "new_pool","id": pool_id, "public_key": pub_key,"denomination": float(denomination), "peers": int(peers), "timeout": int(time.time())+timeout, "relay": self.config.get('nostr_relay', 0)}
        channel_creds = channel
        channel_creds["private_key"] = private_key.hex()     

        pool_path = os.path.expanduser(os.path.join('~', '.electrum', "pools.json"))
    
        if os.path.exists(pool_path) and os.stat(pool_path).st_size > 0:
            with open(pool_path, 'r') as file:
                pools = json.load(file)
        else:
            pools = []

        pools.append(channel_creds)

        with open(pool_path, 'w') as file:
            json.dump(pools, file, indent=2)

        self.publish_event(channel, 10942)

        thread = threading.Thread(target=self.run_checkevents, args=('pool_msg', None, None, None, None, pub_key))
        thread.daemon = True
        thread.start()

    def join_channel(self, request, pubkey, relay):
        event = self.publish_event(request, 4, pubkey, relay=relay)
        
        if not hasattr(self, 'dialog'):
            self.dialog = QDialog()
            self.dialog.setWindowTitle("Pool Request")
            self.dialog_layout = QVBoxLayout()
            self.dialog.setLayout(self.dialog_layout)

            self.message_label = QLabel("Waiting for pool credentials...")
            self.message_label.setAlignment(Qt.AlignCenter)
            self.dialog_layout.addWidget(self.message_label)

            self.progress_bar = QProgressBar()
            self.progress_bar.setTextVisible(False)
            self.dialog_layout.addWidget(self.progress_bar)

            self.dialog.setFixedWidth(400)

            self.dialog.show()

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_progress)
        self.timer.start(100)

        thread = threading.Thread(target=self.run_checkevents, args=('own_msg', None, None, None, None, event.public_key))
        thread.daemon = True
        thread.start()

    def update_progress(self):
        value = self.progress_bar.value()
        value += 1
        if value > 100:
            value = 0
        self.progress_bar.setValue(value)

    def decrypt_msg(self, event_msg, private_key_hex):
 
        private_key_bytes = bytes.fromhex(private_key_hex)
        private_key = PrivateKey(private_key_bytes)

        msg = private_key.decrypt_message(event_msg.event.content, event_msg.event.public_key)

    def share_credentials(self, msg, credentials, pubkey, relay):

        if "{'type': 'join_pool'}" in msg:

            self.publish_event(credentials, 4, pubkey, relay=relay)

    def publish_event(self, data, kind, recipient_pubkey=None, relay=None, wallet=None):
        
        if relay == None:
            relay = self.config.get('nostr_relay', 0)

        relay_manager = RelayManager()
        relay_manager.add_relay(relay)
        
        time.sleep(1.25) 

        private_key = PrivateKey()

        if kind == 4:
            event = EncryptedDirectMessage(recipient_pubkey=recipient_pubkey, cleartext_content=str(data))
        else:
            event = Event(content=json.dumps(data), public_key=private_key.public_key.hex(), kind=kind)

        private_key.sign_event(event)
        relay_manager.publish_event(event)
        time.sleep(2)

        if data["type"] == 'output':
            print(f"[joinstr plugin] Output registered for coinjoin. Event ID: {event.id}")
            QMessageBox.information(self.address_text_fields[wallet].parent(), "Output Registration", f"Output registered for coinjoin\nEvent ID: {event.id}")

        elif data["type"] == 'input':
            print(f"[joinstr plugin] Signed input registered for coinjoin. Event ID: {event.id}")
            QMessageBox.information(self.address_text_fields[wallet].parent(), "Input Registration", f"Signed input registered for coinjoin\nEvent ID: {event.id}")

        elif data["type"] == 'new_pool':
            print(f"[joinstr plugin] New pool created. Event ID: {event.id}")
            QMessageBox.information(None, "Pools", f"New pool created\nEvent ID: {event.id}")

        elif data["type"] == 'join_pool':
            print(f"[joinstr plugin] Join request sent. Event ID: {event.id}")

        relay_manager.close_all_relay_connections()

        return event

    def getevents(self,event_type, event_kind, pubkey=None):

        letters = string.ascii_lowercase
        random_id = ''.join(random.choice(letters) for i in range(10))

        self.saved_relay = self.config.get('nostr_relay', 0)

        filters = Filters([Filter(pubkey_refs=[pubkey], kinds=[event_kind])] if pubkey is not None else [Filter(kinds=[event_kind])])
        subscription_id = random_id
        request = [ClientMessageType.REQUEST, subscription_id]
        request.extend(filters.to_json_array())

        relay_manager = RelayManager()
        relay_manager.add_relay(self.saved_relay)
        relay_manager.add_subscription_on_all_relays(subscription_id, filters)
        time.sleep(1.25)

        pool_list = []
        pool_msgs = []
        own_msgs =  []
        output_list = []
        input_list = []
  
        i = 0

        while relay_manager.message_pool.has_events():
            event_msg = relay_manager.message_pool.get_event()
            event = json.loads(event_msg.event.content) if event_kind != 4 else event_msg.event
            if event_type == 'new_pool':
                try:
                    pool_data = {
                        "id": event['id'],
                        "public_key": event['public_key'],
                        "denomination": event['denomination'],
                        "peers": event['peers'],
                        "timeout": event['timeout'],
                        "relay": event['relay']
                    }
                    pool_list.append(pool_data)
                except:
                    continue
            elif event_type == 'pool_msg':
                try:
                    pool_msgs.append(event)
                except:
                    continue
            elif event_type == 'own_msg':
                try:
                    own_msgs.append(event)
                except:
                    continue
            elif event_type == 'output':
                try:
                    output_list.append(event['address'])
                except:
                    continue
            elif event_type == 'input':
                try:
                    input_list.append(event['hex'])
                except:
                    continue

            i = i + 1

        relay_manager.close_subscription_on_all_relays(subscription_id)
        relay_manager.close_all_relay_connections()


        return pool_list, pool_msgs, output_list, input_list, i

    def checkevents(self, event_type, event_kind, pubkey=None):

        '''
        TODO: Remove duplicates and incorrect bitcoin addresses from output_list
        ''' 

        while True:
            time.sleep(30)

            pool_list, pool_msgs, output_list, input_list, num_i = self.getevents(event_type, event_kind, pubkey=pubkey)

            if num_i != 0:
                return pool_list, pool_msgs, output_list, input_list, num_i

    def show_inputlist(self, tab_widget, input_registration_widget, wallet, label):

        layout = input_registration_widget.layout()

        label.setVisible(False)

        select_label = QLabel("Select an input from the list:")
        select_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(select_label)

        input_list = QListWidget()

        for utxo in wallet.get_utxos():
            item = QListWidgetItem(str(utxo.prevout.txid.hex() + ":" + str(utxo.prevout.out_idx)))
            item.setData(Qt.UserRole, utxo)
            input_list.addItem(item)
        layout.addWidget(input_list)

        register_button = QPushButton("Register")
        layout.addWidget(register_button)
        register_button.setFixedWidth(100)

        layout.setAlignment(register_button, Qt.AlignCenter)

        def on_register_button_clicked():
            selected_items = input_list.selectedItems()
            if selected_items:
                selected_item = selected_items[0]
                selected_input = selected_item.data(Qt.UserRole)
                self.register_input(wallet, selected_input, tab_widget)

        register_button.clicked.connect(on_register_button_clicked)

        tab_widget.update()


    def register_output(self, wallet, address, tab_widget):

        event_type ='output'

        address = self.address_text_fields[wallet].text()
        data = {"address": address, "type": "output"}
        kind=10740
        self.publish_event(data, kind, wallet=wallet)

        self.address_text_fields[wallet].setEnabled(False)

        input_registration_widget = QWidget()
        layout = QVBoxLayout(input_registration_widget)

        label = QLabel("Waiting for other users to register outputs...")
        label.setAlignment(Qt.AlignCenter)

        layout.addWidget(label)

        tab_widget.addTab(input_registration_widget, "Input Registration")

        thread = threading.Thread(target=self.run_checkevents, args=(event_type, tab_widget, input_registration_widget, wallet, label))
        thread.daemon = True
        thread.start()
    
    def run_checkevents(self, event_type, tab_widget=None, input_registration_widget=None, wallet=None, label=None, pubkey=None):

        if event_type == 'output':
            output_list = self.checkevents('output')
            self.signal_emitter.show_inputlist_signal.emit(tab_widget, input_registration_widget, wallet, label)
        elif event_type == 'input':
            input_list = self.checkevents('input')
            final_tx = self.finalize_coinjoin()
        elif event_type == 'own_msg':
            own_msgs = pool_msgs = self.checkevents('own_msg',4,pubkey=pubkey)
            print(own_msgs)
        elif event_type == 'pool_msg':
            pool_path = os.path.expanduser(os.path.join('~', '.electrum', 'pools.json'))

            if os.path.exists(pool_path) and os.stat(pool_path).st_size > 0:
                with open(pool_path, 'r') as file:
                    try:
                        my_pools_data = json.load(file)
                        my_pools_data.reverse()
                    except Exception as e:
                        print(f"Error loading pools from JSON: {e}")
                        my_pools_data = []
                        last_entry = my_pools_data[0]
            credentials = {"id": last_entry["id"],
                           "public_key": last_entry["public_key"],
                           "denomination": last_entry["denomination"],
                           "peers": last_entry["peers"],
                           "timeout": last_entry["timeout"],
                           "relay": last_entry["relay"],
                           "private_key": last_entry["private_key"]
                          } 

            pool_msgs = self.checkevents('pool_msg',4,pubkey=pubkey)
            last_msg = max(pool_msgs, key=lambda x: x.created_at)
            self.share_credentials(last_msg, credentials, last_msg.public_key, last_entry["relay"])
                
    def register_input(self, wallet, selected_input, tab_widget):

        event_type = 'input'

        sighash_type = 0x01 | 0x80  

        '''
        Create PSBT with our input and all registered outputs
        '''
        prevout = selected_input.prevout
        txin = PartialTxInput(prevout=prevout)
        txin._trusted_value_sats = selected_input.value_sats()

        '''
        TODO: Output amount will be based on pool denomination
        '''

        outputs = []
        amount = 1000000

        for address in self.output_list:
            outputs.append((address, amount))

        txout = [PartialTxOutput.from_address_and_value(address, int(amount_btc)) for address, amount_btc in outputs]

        self.tx = PartialTransaction.from_io([txin], txout)

        for txin in self.tx.inputs():
            txin.sighash = sighash_type

        signed_tx = wallet.sign_transaction(self.tx, None)

        serialized_psbt = signed_tx.serialize_as_bytes(force_psbt=True)
        base64_psbt = base64.b64encode(serialized_psbt).decode('ascii')

        data = {"hex": str(base64_psbt), "type": "input"}
        kind = 10740
        self.publish_event(data,kind,wallet)

        finalization_widget = QWidget()
        layout = QVBoxLayout(finalization_widget)

        label = QLabel("Waiting for other users to register signed inputs...")
        label.setAlignment(Qt.AlignCenter)

        layout.addWidget(label)

        tab_widget.addTab(finalization_widget, "Finalization")

        thread = threading.Thread(target=self.run_checkevents, args=(event_type, tab_widget, finalization_widget, wallet, label))
        thread.daemon = True
        thread.start()

        return base64_psbt

    def finalize_coinjoin(self):

        coinjoin_tx = PartialTransaction()

        for psbt_64 in self.input_list:
            psbt = PartialTransaction.from_raw_psbt(psbt_64)
            coinjoin_tx.add_inputs(list(psbt.inputs()))
            existing_outputs = set((out.address, out.value) for out in coinjoin_tx.outputs())
            new_outputs = [out for out in psbt.outputs() if (out.address, out.value) not in existing_outputs]
            coinjoin_tx.add_outputs(new_outputs)

        coinjoin_tx = coinjoin_tx.serialize()

        print(f"[joinstr plugin] Coinjoin tx: {coinjoin_tx}")

        return coinjoin_tx